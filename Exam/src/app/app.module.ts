import { UpdateFormComponent } from './cars/update-form/update-form.component';
import { CarsFormComponent } from './cars/cars-form/cars-form.component';
import { CarsService } from './cars/cars.service';
import { CarsComponent } from './cars/cars.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import {environment} from './../environments/environment';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    CarsComponent,
    LoginComponent,
    NotFoundComponent,
    CarsFormComponent,
    UpdateFormComponent,
    
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {path:'', component:CarsComponent}, //default - localhost:4200 - homepage
      {path:'login', component:LoginComponent}, //localhost:4200/products
      {path:'update-form/:id', component:UpdateFormComponent},
      {path:'**', component:NotFoundComponent} //all the routs that donwt exist
    ])
  ],
  providers: [
    CarsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
