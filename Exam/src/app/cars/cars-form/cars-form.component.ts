import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CarsService } from './../cars.service';
import {FormGroup, FormControl} from '@angular/forms';
@Component({
  selector: 'cars-form',
  templateUrl: './cars-form.component.html',
  styleUrls: ['./cars-form.component.css']
})
export class CarsFormComponent implements OnInit {
  @Output() addCar: EventEmitter<any> = new EventEmitter<any>();
  @Output() addCarPs: EventEmitter<any> = new EventEmitter<any>();

  service:CarsService;
  carform = new FormGroup({
    manufacturer: new FormControl(),
    model: new FormControl()
  });

  sendData(){
    this.addCar.emit(this.carform.value.name);
    this.service.postCar(this.carform.value).subscribe(response =>{
      this.addCarPs.emit();
      console.log(response);
    });
  }
  constructor() { }

  ngOnInit() {
  }

}
