import { CarsService } from './cars.service';
import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router"; //Login without JWT
@Component({
  selector: 'cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {
  cars;
  carsKey;

  optimisticAdd(cars){
    var newKey = this.carsKey[this.carsKey.length-1]+1;
    console.log(this.carsKey.length);
    console.log(this.carsKey);
    var newCarObject = {};
    newCarObject['body'] = cars;
    console.log(newCarObject);
    this.cars[newKey] = newCarObject;
    this.carsKey = Object.keys(this.cars);
    console.log(this.cars);
    console.log(event,newKey);
  }

  pessimiaticAdd(){
    this.service.getCars().subscribe(response => {
      this.cars =  response.json();
      this.carsKey = Object.keys(this.cars);
    });   
  }

   deleteCar(key){
    console.log(key);
    let index = this.carsKey.indexOf(key);
    this.carsKey.splice(index,1);
    //delete from server
    this.service.deleteCar(key).subscribe(
      response=>console.log(response)
    );
  }


 constructor(private service:CarsService, private router:Router) {
    this.service.getCars().subscribe(response=>{
      this.cars = response.json();
      this.carsKey = Object.keys(this.cars);
    });
   }
   //Login without JWT
  logout(){ 
      localStorage.removeItem('auth');      
      this.router.navigate(['/login']);
  }
  ngOnInit() {
  }

}
