import { environment } from './../../environments/environment.prod';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { Headers } from '@angular/http';
import {AngularFireDatabase} from 'angularfire2/database';
@Injectable()
export class CarsService {
  http:Http;
  getCars(){
    //get users from the SLIM rest API (Don't say DB)
    return  this.http.get('http://localhost/angular/Exam/slim/cars');
  }
  postCar(data){
    let options = {
      headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}
    )};
    var params = new HttpParams().append('manufacturer',data.manufacturer).append('model',data.model);
    return this.http.post('http://localhost/angular/Exam/slim/cars',params.toString(),options); 
  }

  deleteCar(key){
    return this.http.delete('http://localhost/angular/Exam/slim/cars/'+key);
  }

  getCar(id) {
    return this.http.get('http://localhost/angular/Exam/slim/cars/'+id);
  }

  putCar(data,key){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('manufacturer',data.manufacturer).append('model',data.model);
    return this.http.put('http://localhost/angular/Exam/slim/cars/'+ key,params.toString(), options);
  }
  login(credentials){
     let options = {
        headers:new Headers({
         'content-type':'application/x-www-form-urlencoded'
        })
     }
    let  params = new HttpParams().append('name', 'jack').append('password','1234');
    return this.http.post('http://localhost/angular/Exam/slim/login', params.toString(),options);
  }



  constructor(http:Http) { 
    this.http = http;
  }

}
