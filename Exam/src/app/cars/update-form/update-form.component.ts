import { CarsService } from './../cars.service';
import { Component, OnInit, EventEmitter, Output  } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'update-form',
  templateUrl: './update-form.component.html',
  styleUrls: ['./update-form.component.css']
})
export class UpdateFormComponent implements OnInit {
  @Output() addCar:EventEmitter<any> = new EventEmitter<any>(); //any is the type. could be [] or {}
  @Output() addCarPs:EventEmitter<any> = new EventEmitter<any>(); //pessimistic

  car;
  manufacturer;
  model;
  service:CarsService;
  carform = new FormGroup({
    manufacturer:new FormControl(),
    model:new FormControl(),
  });
  constructor(private route: ActivatedRoute ,service: CarsService, private router: Router) {
    this.service = service;
   }
   sendData() { //הפליטה של העדכון לאב
    this.addCar.emit(this.carform.value.name);
    console.log(this.carform.value);
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      this.service.putCar(this.carform.value, id).subscribe(
        response => {
          console.log(response.json());
          this.addCarPs.emit();
          this.router.navigateByUrl("/");//return to main page
        }
      );
    })
  }
  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      console.log(id);
      this.service.getCar(id).subscribe(response=>{
        this.car = response.json();
        console.log(this.car);
        //Q4 fix
        this.manufacturer = this.car.manufacturer
        this.model = this.car.model  
      })
    });
   
  }

}
