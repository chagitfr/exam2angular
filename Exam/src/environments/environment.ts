// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url: 'http://localhost/angular/Exam/Slim/',
  firebase:{
    apiKey: "AIzaSyD510ndSvtZwomS4rwmBgdEQoR2lI0SecU",
    authDomain: "exam2-95a9b.firebaseapp.com",
    databaseURL: "https://exam2-95a9b.firebaseio.com",
    projectId: "exam2-95a9b",
    storageBucket: "exam2-95a9b.appspot.com",
    messagingSenderId: "1006725980923"
  }
};
